const { Router } = require('express');
const nodemailer = require('nodemailer');
const router = Router();

router.post('/send-email', async (req, res) => {
  const { name, email, phone, message } = req.body;
  contentHTML = `
        <h1>User Information</h1>
        <ul>
            <li>Username: ${name}</li>
            <li>User Email: ${email}</li>
            <li>Phone: ${phone}</li>
        </ul>
        <p>${message}</p>
        `;
  const transporter = nodemailer.createTransport({
    host: 'mail.draftmailingservice.com.ar',
    port: 25,
    secure: false,
    auth: {
      user: 'testing@draftmailingservice.com.ar',
      pass: '$Strudgrunge351',
    },
    tls: {
      rejectUnauthorized: false,
    },
  });
  const info = await transporter.sendMail({
    from: "'Julio Server' <testing@draftmailingservice.com.ar>",
    to: 'jvdesarrollo@gmail.com',
    subject: 'Website contact form',
    html: contentHTML,
  });
  console.log('Message Send', info.messageId);
  res.redirect('/success.html');
});
module.exports = router;
